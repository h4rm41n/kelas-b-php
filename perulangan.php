<?php

    // perulangan for

    // batas atas
    // batas bawah

    // ulang angka 1 s/d 10
    // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    // 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
    
    // increment  = $a++
    // decrement  = $a--

    for($a=0; $a<10; $a++)
    {
        echo ($a+1).", ";
    }

    for($a=10; $a > 0; $a--)
    {
        echo $a.", ";
    }

?>